question1<-function(){
 dt<-read.csv("/home/ermira/Documents/survey1.csv",skip=2,header=F)
 View(dt)
  barplot(c(mean(dt[,14]),mean(dt[,15]),mean(dt[,16]),mean(dt[,17]),mean(dt[,18])),
  ylab='Frequency',ylim=c(0,60),col=c("yellow","orange",'green','blue','violet'),xlab="Time spent",legend=c('Write new code','Test code','Debugg','Refactor code','Other'))
  }
question2<-function(){
 dt<-read.csv("/home/ermira/Documents/survey1.csv",skip=2,header=F)
 barplot(c(length(which(x=="0%-10%")),length(which(x=="11%-20%")),length(which(x=="21%-30%"))
 ,length(which(x=="31%-40%")),length(which(x=="41%-50%")),length(which(x=="51%-60%")),length(which(x=="61%-70%")),length(which(x=="71%-80%")),
 length(which(x=="81%-90%")),length(which(x=="91%-100%"))))
}
getImportanceNum <- function(x) {
  if(x == "Very important") {
    return(5)
  }
  else if(x == "Important") {
    return(4)
  }
  else if(x == "Moderately important") {
    return(3)
  }
  else if(x == "Of little importance") {
    return(2)
  }
  else {
    return(1)
  }
}
question3<-function(){
Q1 <- unlist(lapply(dt$V20, getImportanceNum))
  Q2 <- unlist(lapply(dt$V21, getImportanceNum))
  Q3 <- unlist(lapply(dt$V22, getImportanceNum))
  Q4 <- unlist(lapply(dt$V23, getImportanceNum))
  Q5 <- unlist(lapply(dt$V24, getImportanceNum))
  Q6 <- unlist(lapply(dt$V25, getImportanceNum))
  Q7 <- unlist(lapply(dt$V26, getImportanceNum))
boxplot(Q1,Q2,Q3,Q4,Q5,Q6,Q7)
}

 getImportanceNum <- function(x) {
 if(x=="First"){
 return(1)
 }
 else if(x=="Second"){
 return(2)
 }
 else if(x=="Third"){
 return(3)
 }
 else if(x=="Fourth"){
 return(4)
 }
 else if (x=="Fifth"){
 return(5)
 }
 else if(x=="Sixth"){
 return(6)
 }
 else if(x=="Seventh"){
 return(7)
 }
 }
question4<-function(){
Q1 = c(length(dt$V20[dt$V27 == "Seventh"]),
         length(dt$V20[dt$V27 == "Sixth"]),
         length(dt$V20[dt$V27 == "Fifth"]),
         length(dt$V20[dt$V27 == "Fourth"]),
         length(dt$V20[dt$V27 == "Third"]),
         length(dt$V20[dt$V27 == "Second"]),
         length(dt$V20[dt$V27 == "First"])
  )
  
  Q2 = c(length(dt$V20[dt$V28 == "Seventh"]),
         length(dt$V20[dt$V28 == "Sixth"]),
         length(dt$V20[dt$V28 == "Fifth"]),
         length(dt$V20[dt$V28 == "Fourth"]),
         length(dt$V20[dt$V28 == "Third"]),
         length(dt$V20[dt$V28 == "Second"]),
         length(dt$V20[dt$V28 == "First"])
  )
  Q3 = c(length(dt$V20[dt$V29 == "Seventh"]),
         length(dt$V20[dt$V29 == "Sixth"]),
         length(dt$V20[dt$V29 == "Fifth"]),
         length(dt$V20[dt$V29 == "Fourth"]),
         length(dt$V20[dt$V29 == "Third"]),
         length(dt$V20[dt$V29 == "Second"]),
         length(dt$V20[dt$V29 == "First"])
  )
  Q4 = c(length(dt$V20[dt$V30 == "Seventh"]),
         length(dt$V20[dt$V30 == "Sixth"]),
         length(dt$V20[dt$V30 == "Fifth"]),
         length(dt$V20[dt$V30 == "Fourth"]),
         length(dt$V20[dt$V30 == "Third"]),
         length(dt$V20[dt$V30 == "Second"]),
         length(dt$V20[dt$V30 == "First"])
  )
  Q5 = c(length(dt$V20[dt$V31 == "Seventh"]),
         length(dt$V20[dt$V31 == "Sixth"]),
         length(dt$V20[dt$V31 == "Fifth"]),
         length(dt$V20[dt$V31 == "Fourth"]),
         length(dt$V20[dt$V31 == "Third"]),
         length(dt$V20[dt$V31 == "Second"]),
         length(dt$V20[dt$V31 == "First"])
  )
  Q6 = c(length(dt$V20[dt$V32 == "Seventh"]),
         length(dt$V20[dt$V32 == "Sixth"]),
         length(dt$V20[dt$V32 == "Fifth"]),
         length(dt$V20[dt$V32 == "Fourth"]),
         length(dt$V20[dt$V32 == "Third"]),
         length(dt$V20[dt$V32 == "Second"]),
         length(dt$V20[dt$V32 == "First"])
  )
  Q7 = c(length(dt$V20[dt$V33 == "Seventh"]),
         length(dt$V20[dt$V33 == "Sixth"]),
         length(dt$V20[dt$V33 == "Fifth"]),
         length(dt$V20[dt$V33 == "Fourth"]),
         length(dt$V20[dt$V33 == "Third"]),
         length(dt$V20[dt$V33 == "Second"]),
         length(dt$V20[dt$V33 == "First"])
  )
boxplot(Q1,Q2,Q3,Q4,Q5,Q6,Q7)
}
